#include<iostream>
#include<cmath>
#include<stdexcept>//包含标准异常判断库
using namespace std;
// 创建一个点类
class Point
{
    public:
    Point()
    {   // 默认构造(无参构造)
        cout<<"Power默认构造(无参构造)调用"<<endl;
    }
    Point(int temp_x,int temp_y)  // 有参构造
    {
        x=temp_x;
        y=temp_y;
        cout<<"Power有参构造调用"<<endl;
    }
    Point(const Point&p) //const:是为了避免改变p
    {   
        x=p.x;
        y=p.y;
        cout<<"Power拷贝(复制)函数调用"<<endl;  
    }
    //以上这些无聊的玩意都属于函数重载
    void set_x(int temp_x)
    {
        x=temp_x;
    }
    void set_y(int temp_y)
    {
        y=temp_y;
    }
    int get_x()
    {
        return x;
    }
    int get_y()
    {
        return y;
    }
    ~Point()
    {
        cout<<"Power析构函数调用"<<endl;
    }    
    private :
    int x;
    int y;
};
// 接着使用Power类套一个线段类
class Line{
    public:
    Line(Point temp_p1,Point temp_p2) //有参构造
    {
        p1=temp_p1;
        p2=temp_p2;
        cout<<"Line的有参构造调用"<<endl;
    }
    ~Line() //析构
    {
        cout<<"Line的析构函数调用"<<endl;
    }
    Line(const Line &temp_L)  // 拷贝(复制)
    {
        p1=temp_L.p1;
        p2=temp_L.p2;
         cout<<"Line的拷贝构造函数调用"<<endl;
    }
    void to_show()
    {
        cout<<"The_Point_One:  "<<'('<<p1.get_x()<<','<<p1.get_y()<<')'<<endl;
        cout<<"The_Point_Two:  "<<'('<<p2.get_x()<<','<<p2.get_y()<<')'<<endl;
        cout<<"The_Length:  "<<Length<<endl;

    }
    void change_Point(const Point &p,int i)
    {   
        if(i==1)
        {
            p1=p;
        }
       else if(i==2)
        {
            p2=p;
        }
        else 
        {
            cerr<<"Warning:The parameter must be 1 or 2"<<endl;
        }
    }
    void change_Point(int x,int y,int i)  //重载一下，具有两种更改方式
    {
        if (i==1)
        {
            p1.set_x(x);
            p1.set_y(y);
        }
         else if (i==2)
        {
            p2.set_x(x);
            p2.set_y(y);
        }
        else{
            cerr<<"Warning:The parameter must be 1 or 2"<<endl;
        }
    }
    private:
    Point p1;
    Point p2; // 线段都要有两个端点对吧
    double Length=sqrt(pow((p1.get_x() - p2.get_x()),2)+pow((p1.get_y()-p2.get_y()),2));
};
int gain_parameter(void)
{   int i;
    cin>>i;
    return i;
} 
// 如何写了一个拷贝构造函数，则编译器就不提供其他的普通构造函数，意味着无法进行普通的对象定义
// 比如Point p1  或者Point p2=Point(0,0) 这就会进行报错了
// 其实问题的本身就是函数的重载 写了Point构造函数，则系统就不提供其他的
int main()
{
    Point p1(gain_parameter(),gain_parameter());//从键盘得到点
    Point p2(gain_parameter(),gain_parameter());
    Line L1=Line(p1,p2);
    L1.to_show();
    L1.change_Point(gain_parameter(),gain_parameter(),gain_parameter());
    L1.change_Point(gain_parameter(),gain_parameter(),gain_parameter());
    L1.to_show();
    Point p3=p2; 
    return 0;
}

