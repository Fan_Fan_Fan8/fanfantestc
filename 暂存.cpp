#include<iostream>
#include<math.h>
using namespace std;
//定义一个时间类
class Clock {
public:
    Clock(int hour_ = 0, int minute_ = 0, int second_ = 0);
    void timeshow()
    {
        cout << hour << ":" << minute << ":" << second;
    }
private:
    int hour, minute, second;
};
//构造函数
Clock::Clock(int hour_, int minute_, int second_)
{
    hour = hour_;
    minute = minute_;
    second = second_;
};
class Circle {
public:
    Circle(double rr) :r(rr) {};
    void set_radius(double rr)
    {
        r = rr;
    }
    double get_radius()
    {
        return r;
    }
    double area_to_show()
    {
        return M_PI * r * r;
    }
private:
    double r;
};
int main()
{
    Clock c1(12, 30, 45);
    c1.timeshow();
    cout << endl;
    Circle c2(3.0), c3(4.0);
    c2.set_radius(5.0);
    cout<<c2.area_to_show()<<endl;
    cout << c3.area_to_show();
    return 0;
}